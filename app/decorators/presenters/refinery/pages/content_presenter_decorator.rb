Refinery::Pages::ContentPresenter.class_eval do
  def to_html(can_use_fallback = true)
        sections_html(can_use_fallback)
    end
end